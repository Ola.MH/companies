<?php
class Travel
{
    private $travelsList; 

    private static $instance = null;
      
    private function __construct()
    {
        $this->travelsList = $this->getTravelsListAPI();
    }
     
    public static function getInstance()
    {
        if (self::$instance == null)
        {
          self::$instance = new Travel();
        }
     
        return self::$instance;
    }

   public function getTravelsListAPI(){

        $json = file_get_contents('https://5f27781bf5d27e001612e057.mockapi.io/webprovise/travels');

        $data = json_decode($json,true);
        
        return $data;
   }

   public function getTravelsList(){

        return $this->travelsList;
   }

   public function getCopmanyTravelsCost($company_id){
        $cost = 0;
        $list = $this->getTravelsList();
        if($list){
            foreach ($list as $key => $travel) {
                if($travel['companyId'] == $company_id){
                    $cost += $travel['price'];
                }
            }
        }
        return $cost;    
   } 
}


class Company
{
    private $companiesList; 

    public function __construct()
    {
        $this->companiesList = $this->getCompaniesListAPI();
    }

    public function getCompaniesListAPI(){
        $json = file_get_contents('https://5f27781bf5d27e001612e057.mockapi.io/webprovise/companies');
  
        $data = json_decode($json,true);

        return $data;
    }

    public function getcompaniesList(){

        return $this->companiesList;
    }

    public function getNestedCompaniesList(){

        $nestedCompaniesList = $this->_buildCompaniesTreeArray($this->getcompaniesList()); 

        return $nestedCompaniesList;  
   }

    private  function _buildCompaniesTreeArray(array $companiesList, $parentId = 0){
        $branch = array();
        $travel = Travel::getInstance();
        $cost = 0;

        foreach ($companiesList as $element) {
            if ($element['parentId'] == $parentId) {
                if($element['parentId'] == 0) $cost = 0;
                $children = $this->_buildCompaniesTreeArray($companiesList, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                    foreach ($children as $child) {
                        $cost += $child['cost']; 
                    }
                }
                $element['cost'] = $travel->getCopmanyTravelsCost($element['id']) + $cost;
                $branch[] = $element;
            }
        }

        return $branch;
   }
}
class TestScript
{
    public function execute()
    {
        $start = microtime(true);
        
        $companieswithTraverCosts = (new Company())->getNestedCompaniesList();
        
        echo json_encode($companieswithTraverCosts);
      
        echo 'Total time: '.  (microtime(true) - $start);
    }
}
(new TestScript())->execute();